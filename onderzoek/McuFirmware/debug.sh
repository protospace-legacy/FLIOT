#!/bin/sh
echo Starting OPENOCD
nohup openocd -f /usr/share/openocd/scripts/interface/stlink-v2.cfg -f /usr/share/openocd/scripts/target/stm32f1x.cfg &
openocdpid=$!

arm-none-eabi-gdb .pioenvs/bluepill/firmware.elf -x startup.gdb
gdbpid=$!
wait -n $gdbpid

kill $openocdpid
