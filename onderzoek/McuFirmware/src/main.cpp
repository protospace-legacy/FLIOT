#include <Arduino.h>
#include <system_stm32f1xx.h>
template <class T>
inline Print &operator<<(Print &obj, T arg)
{
  obj.print(arg);
  return obj;
}

#include <SPI.h>
#include "BoardController.hpp"

void master_loop()
{
  MasterBC bc;
  Serial.begin(9600);
  while(true){
    bc.report();
    delay(100);
  }

}

void slave_loop(){
  digitalWrite(LED_BUILTIN, HIGH);
  delay(1000);
  SlaveBC bc;
  while(true){
    bc.update();
  }
}

void setup()
{
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(PB2, INPUT);

  if (digitalRead(PB2)){
    master_loop();
  } else{
    slave_loop();
  }
}

void loop() {}