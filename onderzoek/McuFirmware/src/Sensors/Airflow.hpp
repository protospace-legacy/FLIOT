#pragma once
#include "sensor.hpp"
#include "Arduino.h"

class AirFlowSensor : public Sensor<uint_fast16_t>
{
    const size_t analogPin;

  public:
    AirFlowSensor(size_t fanalogPin) : Sensor("Airflow","cm/s"), analogPin(fanalogPin)
    {
        pinMode(analogPin, INPUT);
    }

    value_t get() const{
        return analogRead(analogPin);
    }
};