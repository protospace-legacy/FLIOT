#pragma once
#include "sensor.hpp"
#include "Arduino.h"

class Co2 : public Sensor<uint_fast16_t>
{
    const size_t analogPin;

  public:
    Co2(size_t analogPin) : Sensor("Co2", "ppm"), analogPin(analogPin)
    {
        pinMode(analogPin, INPUT);
    }
    value_t get() const
    {
        return analogRead(analogPin);
    }
};