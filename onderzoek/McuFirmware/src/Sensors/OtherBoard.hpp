#pragma once
#include <Arduino.h>
#include <SPI.h>

#include "sensor.hpp"

#include "Airflow.hpp"
#include "BMP280.hpp"
#include "Co.hpp"
#include "Co2.hpp"
#include "Particle.hpp"
#include "Reflectie.hpp"

SPIClass SPI_2(PB15, PB14, PB13); //Create an SPI2 object.

struct OtherBoardData : public Printable
{
    uint8_t magic = 0x0B;
    AirFlowSensor::value_t airflow;
    BMP280_Pressure::value_t pressure;
    BMP280_Temperature::value_t temperature;
    Co::value_t co;
    Co2::value_t co2;
    ParticleSensor::value_t particles;
    ReflectieSensor::value_t reflection;

    OtherBoardData(
        AirFlowSensor::value_t airflow,
        BMP280_Pressure::value_t pressure,
        BMP280_Temperature::value_t temperature,
        Co::value_t co,
        Co2::value_t co2,
        ParticleSensor::value_t particles,
        ReflectieSensor::value_t reflection) : airflow(airflow),
                                               pressure(pressure),
                                               temperature(temperature),
                                               co(co),
                                               co2(co2),
                                               particles(particles),
                                               reflection(reflection)
    {
    }
    size_t printTo(Print &p) const
    {
        size_t r = 0;
        r += p.print(airflow);
        r += p.print("\t");
        r += p.print(pressure);
        r += p.print("\t");
        r += p.print(temperature);
        r += p.print("\t");
        r += p.print(co);
        r += p.print("\t");
        r += p.print(co2);
        r += p.print("\t");
        r += p.print(particles);
        r += p.print("\t");
        r += p.print(reflection);
        r += p.print("\t");
        return r;
    }
};

class OtherBoard : public Sensor<OtherBoardData>
{
  public:
    OtherBoard() : Sensor("Other board", ""){};
    OtherBoardData get() const
    {
    SPI_2.begin();
    SPI_2.beginTransaction(SPISettings(F_CPU/256, MSBFIRST, SPI_MODE0));
    uint8_t buffer[sizeof(OtherBoardData)] = {0,};
    size_t timeout = 0;
    while(buffer[0] != 0x0B && timeout<100){
        buffer[0] = SPI_2.transfer(0xFF);
        timeout++;
    }
    if(timeout == 100){
      return OtherBoardData(0,0,0,0,0,0,0);
    }
    for(size_t i = 1; i < sizeof(OtherBoardData)-1; i++){
      buffer[i]= SPI_2.transfer(0x00);
    }
    return *reinterpret_cast<OtherBoardData*>(&buffer);
    }
};