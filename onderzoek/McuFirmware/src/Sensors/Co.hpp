#pragma once
#include "sensor.hpp"
#include "Arduino.h"

class Co : public Sensor<uint_fast16_t>
{
    const size_t analogPin;

  public:
    Co(size_t analogPin) : Sensor("Co", "ppm"), analogPin(analogPin)
    {
        pinMode(analogPin, INPUT);
    }
    value_t get() const
    {
        return analogRead(analogPin);
    }
};