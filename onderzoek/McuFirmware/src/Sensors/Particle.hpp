#pragma once
#include "sensor.hpp"
#include "Arduino.h"

class ParticleSensor : public Sensor<uint_fast16_t>
{
    const size_t analogPin;
    const size_t ledPin;
    value_t v;
  public:
    ParticleSensor(size_t analogPin, size_t ledPin) : Sensor("Particles", "p/cm3"), analogPin(analogPin), ledPin(ledPin)
    {
        pinMode(analogPin, INPUT);
        pinMode(ledPin, OUTPUT);
        digitalWrite(ledPin, HIGH);
    }

    value_t get() const
    {
        digitalWrite(ledPin, LOW);                 // turn the LED off
        delayMicroseconds(280);                     // Give sensor some time to settle
        value_t voMeasured = analogRead(analogPin); // read the dust value
        digitalWrite(ledPin, HIGH);                 // turn the LED off
        return voMeasured;
    }
};