#pragma once
#include "sensor.hpp"
#include "Arduino.h"

class ReflectieSensor : public Sensor<uint_fast16_t>
{
    const size_t analogPin;
    const size_t ledPin;

  public:
    ReflectieSensor(size_t analogPin, size_t ledPin) : Sensor("Reflectie", "%"), analogPin(analogPin), ledPin(ledPin)
    {
        pinMode(analogPin, INPUT);
        pinMode(ledPin, OUTPUT);
    }

    value_t get() const
    {
        digitalWrite(ledPin, HIGH);
        delay(2);
        value_t f = analogRead(analogPin);
        digitalWrite(ledPin, LOW);
        return f;
    }
};