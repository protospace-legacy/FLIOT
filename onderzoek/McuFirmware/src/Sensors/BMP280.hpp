#pragma once
#include "sensor.hpp"
#include "Arduino.h"
#include <Adafruit_Sensor.h>
#include <Adafruit_BMP280.h>

class BMP280_Temperature : public Sensor<uint_fast32_t>
{
    Adafruit_BMP280 &bme;

  public:
    BMP280_Temperature(Adafruit_BMP280 &bme) : Sensor("Temperature", "*C"), bme(bme){};
    value_t get() const
    {
        return bme.readTemperature();
    }
};

class BMP280_Pressure : public Sensor<uint_fast32_t>
{
    Adafruit_BMP280 &bme;

  public:
    BMP280_Pressure(Adafruit_BMP280 &bme) : Sensor("Pressure", "Pa"), bme(bme){};
    value_t get() const
    {
        return bme.readPressure();
    }
};

class BMP280 : public SensorPackage<BMP280_Pressure, BMP280_Temperature>
{
  public:
    Adafruit_BMP280 bme;
    BMP280() : SensorPackage(BMP280_Pressure(bme), BMP280_Temperature(bme))
    {
        if (!bme.begin(0x76))
        {
            Serial.println("Could not find a valid BMP280 sensor, check wiring!");
            while (1);
        }
    };
};