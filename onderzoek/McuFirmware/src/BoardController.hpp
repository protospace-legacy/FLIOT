#pragma once
#include <stm32f1xx_hal.h>
#include <stm32f1xx_hal_dma.h>
#include "Sensors/sensor.hpp"
#include "Sensors/Particle.hpp"
#include "Sensors/Airflow.hpp"
#include "Sensors/Reflectie.hpp"
#include "Sensors/Co2.hpp"
#include "Sensors/Co.hpp"
#include "Sensors/BMP280.hpp"
#include "Sensors/OtherBoard.hpp"


class BoardController
{
protected:
  ReflectieSensor rs;
  AirFlowSensor afs;
  Co2 co2s;
  Co cos;
  ParticleSensor ps;
  BMP280 bmp;

  static const size_t sensorCount = 6;
  SensorBase *sensors[sensorCount] = {&bmp, &ps, &rs, &afs, &co2s, &cos};

public:
  BoardController() : rs(PA0, PB12),
                      afs(PA5),
                      co2s(PA4),
                      cos(PA1),
                      ps(PA6, PB1){};
};

class MasterBC : public BoardController
{
  OtherBoard ob;

public:
  MasterBC() : BoardController(){};
  void report()
  {
    for (uint8_t i = 0; i < sensorCount; i++)
    {
      Serial.print(*sensors[i]);
    }
    Serial.print(ob);
    Serial.println();
  }
};

class SlaveBC : public BoardController
{
  OtherBoardData obd;
  char inData = 0;
public:
  SlaveBC():
    obd(OtherBoardData( 1, 2, 3, 4, 5, 6, 7))
  {
    RCC->APB2ENR |= RCC_APB2ENR_AFIOEN;
    RCC->APB2ENR |= RCC_APB2ENR_IOPBEN;
    GPIOB->CRH =  (0b0011 << GPIO_CRH_MODE12_Pos) |
                  (0b1011 << GPIO_CRH_MODE13_Pos) |
                  (0b1011 << GPIO_CRH_MODE14_Pos) | 
                  (0b1011 << GPIO_CRH_MODE15_Pos);
    
    RCC->APB1ENR |= RCC_APB1ENR_SPI2EN;
    SPI2->CR1 = 0 | (1 << 9) | (1<<6) | (0b111 << 3);
    SPI2->CR2 = SPI_CR2_TXDMAEN | SPI_CR2_RXDMAEN;
    RCC->AHBENR |= RCC_AHBENR_DMA1EN;

    DMA1_Channel5->CMAR = (size_t) &obd;
    DMA1_Channel5->CPAR = (size_t) &SPI2->DR;
    DMA1_Channel5->CCR =  (0b11 << 12) | DMA_CCR_MINC | DMA_CCR_DIR;
}

  void update()
  {
    if(DMA1_Channel5->CNDTR == 0){
      DMA1_Channel5->CCR &= ~DMA_CCR_EN;
      obd = OtherBoardData(
         afs.get(),
         std::get<0>(bmp.sensors).get(),
         std::get<1>(bmp.sensors).get(),
         cos.get(),
         co2s.get(),
         ps.get(),
         rs.get()
      );
      DMA1_Channel5->CNDTR = sizeof(OtherBoardData)/sizeof(uint8_t);
      DMA1_Channel5->CCR |= DMA_CCR_EN;
      }
  }
};