from pathlib import Path
import traceback
import threading
import serial
import argparse
import csv
import datetime
import csv

measurementThread = None

class Measurer(threading.Thread):
    def __init__(self, measurementName, serialPort='/dev/ttyUSB0',  variables=14, baudrate=9600):
        threading.Thread.__init__(self)
        if Path(measurementName + '.csv').exists():
           raise Exception("Measurement already exists")
        self.serial = serialPort
        self.baudrate = baudrate
        self.mname = measurementName
        self.variables = variables

        self.ecsv = None
        self.running = False

    def run(self):
        self.running = True
        mfile = open(self.mname + ".csv", 'w')
        mcsv = csv.writer(mfile)

        efile = open(self.mname + ".event.csv", 'w')
        self.ecsv = csv.writer(efile)

        self.startStamp = datetime.datetime.now()

        with serial.Serial(self.serial, self.baudrate)  as s :
            while self.running:
                i = s.readline().decode('ascii').strip().split('\t')
                if len(i) == self.variables:
                    mcsv.writerow([(datetime.datetime.now() - self.startStamp)] + i)
        mfile.close()
        self.ecsv = None
        efile.close()

    def event(self, eventname):
        if not self.ecsv:
            print("Couldn't add event")
            return
        self.ecsv.writerow([datetime.datetime.now() - self.startStamp, eventname])

    def stop(self):
        self.running = False

def create(args):
    global measurementThread
    if measurementThread:
        print("Measurement already created")
        return
    try:
        measurementThread = Measurer(args.Name)
    except Exception:
        print(traceback.format_exc())

def start(args):
    global measurementThread
    if not measurementThread:
        print("No measurement created")
        return
    measurementThread.start()


def stop(args):
    global measurementThread
    if not measurementThread:
        print("No measurement started")
        return
    measurementThread.stop()
    measurementThread = None


def event(args):
    global measurementThread
    if not measurementThread:
        print("No measurement started")
        return
    measurementThread.event(' '.join(args.event))

def console(parser, subcommands):
    exit_command = subcommands.add_parser('exit', help="Exit this program", aliases=['e', 'q', 'quit'])
    exit_command.set_defaults(func=lambda x: exit())

    help_command = subcommands.add_parser('help', help="Show this help")
    help_command.set_defaults(func=lambda x: parser.print_help())

    parser.usage = argparse.SUPPRESS

    parser.print_help()

    while True:
        command = input("Command: ")
        args = None
        try:
            args = parser.parse_args(command.split())
        except SystemExit:
            pass
        if hasattr(args, 'func'):
            args.func(args)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(add_help=False)
    subcom = parser.add_subparsers(title="Commands")

    create_command = subcom.add_parser('create', help="Create a new meauserment")
    create_command.set_defaults(func=create)
    create_command.add_argument('Name', type=str, help="Name of measurement")

    start_command = subcom.add_parser('start', help="Start measurement")
    start_command.set_defaults(func=start)

    stop_command = subcom.add_parser('stop', help="Stop measurement")
    stop_command.set_defaults(func=stop)

    event_command = subcom.add_parser('event', help='Event')
    event_command.set_defaults(func=event)
    event_command.add_argument('event', type=str, nargs='+', help='Name of event')

    try:
        console(parser, subcom)
    except KeyboardInterrupt:
        print()
    except EOFError:
        print()
