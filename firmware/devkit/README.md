Devkit Source
=============

This is the source directory for the devkit specific sources
The used devkit is a nrf52840 dk (PCA10056).
Flashing is done with nrfjprog.
Softdevice S112 is used to make sure the API is consitent between both the devkit and the platform.
