#include <nrf_gpio.h>

void sleep(size_t cycles){
    for(size_t i = 0; i<cycles; i++){
        asm("nop");
    }
};

int main(){
    const uint32_t led_1_pin =  NRF_GPIO_PIN_MAP(0,13);
    nrf_gpio_cfg_output(led_1_pin);
    while(true){
        nrf_gpio_pin_toggle(led_1_pin);
        sleep(100000);
    }
    return 0;
}
