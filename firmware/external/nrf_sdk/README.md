#NRF_SDK

Download the latest nrf sdk from:
[https://developer.nordicsemi.com/nRF5_SDK/](https://developer.nordicsemi.com/nRF5_SDK/)
Don't forget to update the version in `common/nrf.cmake`
