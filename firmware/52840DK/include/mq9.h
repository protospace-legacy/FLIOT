#ifndef __MQ9_H__
#define __MQ9_H__

#include "nrfx_saadc.h"

typedef struct mq9_config {
    float R0;
    nrf_saadc_input_t input_pin;
    uint8_t channel_nr;
} mq9_config;

#define MQ9_CONFIG(adc_pin, channel) {      \
    .R0 = (float)0.69,                      \
    .input_pin = (adc_pin),                 \
    .channel_nr = (uint8_t)(channel)        \
}


// Initializes the adc channel for use with the sensor
void mq9_init(mq9_config *config);


// Function to calibrate the sensor in clean air
float mq9_get_calibration(mq9_config *config);


// Function to calculate the RS/R0 value
float mq9_get_RSR0(mq9_config *config);


#endif //__MQ9_H__