#ifndef __MQ135_H__
#define __MQ135_H__

#include <math.h>
#include "nrfx_saadc.h"

// The load resistance on the board in ohm
#define RLOAD 1000.0

// Parameters for calculating ppm of CO2 from sensor resistance
#define PARA 116.6020682
#define PARB 2.769034857

// Parameters to model temperature and humidity dependence
#define CORA 0.00035
#define CORB 0.02718
#define CORC 1.39538
#define CORD 0.0018
#define CORE -0.003333333
#define CORF -0.001923077
#define CORG 1.130128205

/// Atmospheric CO2 level for calibration purposes
#define ATMOCO2 408.53

typedef struct mq135_config {
    float R0;
    nrf_saadc_input_t input_pin;
    uint8_t channel_nr;
} mq135_config;

#define MQ135_CONFIG(adc_pin, channel) {    \
    .R0 = (float)28221.19,                  \
    .input_pin = (adc_pin),                 \
    .channel_nr = (uint8_t)(channel)        \
}


// Initializes the adc channel for use with the sensor
void mq135_init(mq135_config *config);


// Function to calculate the ppm of co2 in the air
float mq135_get_ppm(mq135_config *config);


// Function to calculate the ppm of CO2 in the air based on temperature and humidity
float mq135_get_corrected_ppm(mq135_config *config, float t, float h);


// Function to calibrate the sensor at atmospheric CO2 level
float mq135_get_calibration(mq135_config *config);


// Function to calibrate the sensor at atmospheric CO2 level
// based on temperature and humidity
float mq135_get_corrected_calibration(mq135_config *config, float t, float h);


#endif //__MQ135_H__