#ifndef __BMP280_H__
#define __BMP280_H__

#include "nrfx_spim.h"
#include "app_util_platform.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "boards.h"
#include "app_error.h"
#include <string.h>

// Sensor sample modes
enum sensor_sampling {
    // No over-sampling
    SAMPLING_NONE = 0x00,
    // 1x over-sampling
    SAMPLING_X1 = 0x01,
    // 2x over-sampling
    SAMPLING_X2 = 0x02,
    // 4x over-sampling
    SAMPLING_X4 = 0x03,
    // 8x over-sampling
    SAMPLING_X8 = 0x04,
    // 16x over-sampling
    SAMPLING_X16 = 0x05
};

// Operating mode for the sensor
enum sensor_mode {
    // Sleep mode
    MODE_SLEEP = 0x00,
    // Forced mode
    MODE_FORCED = 0x01,
    // Normal mode
    MODE_NORMAL = 0x03,
    // Software reset
    MODE_SOFT_RESET_CODE = 0xB6
};

// Filtering level for sensor data
enum sensor_filter {
    // No filtering
    FILTER_OFF = 0x00,
    // 2x filtering
    FILTER_X2 = 0x01,
    // 4x filtering
    FILTER_X4 = 0x02,
    // 8x filtering
    FILTER_X8 = 0x03,
    // 16x filtering
    FILTER_X16 = 0x04
};

// Standby duration in ms
enum standby_duration {
    // 0.5 ms standby
    STANDBY_MS_1 = 0x00,
    // 62.5 ms standby
    STANDBY_MS_63 = 0x01,
    // 125 ms standby
    STANDBY_MS_125 = 0x02,
    // 250 ms standby
    STANDBY_MS_250 = 0x03,
    // 500 ms standby
    STANDBY_MS_500 = 0x04,
    // 1000 ms standby
    STANDBY_MS_1000 = 0x05,
    // 2000 ms standby
    STANDBY_MS_2000 = 0x06,
    // 4000 ms standby
    STANDBY_MS_4000 = 0x07
};

// bmp280 config register 
typedef struct {
    // Inactive duration (standby time) in normal mode
    unsigned int t_sb : 3;
    // Filter settings
    unsigned int filter : 3;
    // Unused - don't set
    unsigned int none : 1;
    // Enables 3-wire SPI
    unsigned int spi3w_en : 1;
} config;

// Encapsulates trhe ctrl_meas register
typedef struct {
    // Temperature oversampling
    unsigned int osrs_t : 3;
    // Pressure oversampling
    unsigned int osrs_p : 3;
    // Device mode
    unsigned int mode : 2;
} ctrl_meas;

// BMP280 registers
enum {
  BMP280_REGISTER_DIG_T1 = 0x88,
  BMP280_REGISTER_DIG_T2 = 0x8A,
  BMP280_REGISTER_DIG_T3 = 0x8C,
  BMP280_REGISTER_DIG_P1 = 0x8E,
  BMP280_REGISTER_DIG_P2 = 0x90,
  BMP280_REGISTER_DIG_P3 = 0x92,
  BMP280_REGISTER_DIG_P4 = 0x94,
  BMP280_REGISTER_DIG_P5 = 0x96,
  BMP280_REGISTER_DIG_P6 = 0x98,
  BMP280_REGISTER_DIG_P7 = 0x9A,
  BMP280_REGISTER_DIG_P8 = 0x9C,
  BMP280_REGISTER_DIG_P9 = 0x9E,
  BMP280_REGISTER_CHIPID = 0xD0,
  BMP280_REGISTER_VERSION = 0xD1,
  BMP280_REGISTER_SOFTRESET = 0xE0,
  BMP280_REGISTER_CAL26 = 0xE1, // R calibration = 0xE1-0xF0
  BMP280_REGISTER_CONTROL = 0xF4,
  BMP280_REGISTER_CONFIG = 0xF5,
  BMP280_REGISTER_PRESSUREDATA = 0xF7,
  BMP280_REGISTER_TEMPDATA = 0xFA,
};  

// Struct to hold calibration data.
typedef struct {
  uint16_t dig_T1;
  int16_t dig_T2;
  int16_t dig_T3;

  uint16_t dig_P1;
  int16_t dig_P2;
  int16_t dig_P3;
  int16_t dig_P4;
  int16_t dig_P5;
  int16_t dig_P6;
  int16_t dig_P7;
  int16_t dig_P8;
  int16_t dig_P9;
} bmp280_calib_data;

typedef struct bmp280_config {
    uint8_t sck_pin;
    uint8_t mosi_pin;
    uint8_t miso_pin;
    uint8_t ss_pin;
    nrfx_spim_t spi;
    int32_t t_fine;
    bmp280_calib_data calib;
    config config_reg;
    ctrl_meas meas_reg;
} bmp280_config;

#define BMP280_CONFIG(sck, mosi, miso, ss, instance) {   \
    .sck_pin = (sck),                                                \
    .mosi_pin = (mosi),                                              \
    .miso_pin = (miso),                                              \
    .ss_pin = (ss),                                                  \
    .spi = NRFX_SPIM_INSTANCE(instance)                              \
}


// Sets the sampling for the sensor
void bmp280_set_sampling(bmp280_config *config, 
                         uint8_t mode,
                         uint8_t tempSampling,
                         uint8_t pressSampling,
                         uint8_t filter,
                         uint8_t duration);


// Function to calculate the temperature
float bmp280_read_temperature(bmp280_config *config);


// Function to calculate the pressure
float bmp280_read_pressure(bmp280_config *config);


// Initializes SPI connection for the sensor, reads the registers for calibration and sets the sampling
void bmp280_init(bmp280_config *config);


#endif //__BMP280_H__