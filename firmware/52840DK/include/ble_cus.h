#ifndef BLE_CUS__
#define BLE_CUS__

#include <stdint.h>
#include <stdbool.h>
#include "ble.h"
#include "ble_srv_common.h"

// Custom UUIDS used for services and values
#define CUSTOM_SERVICE_UUID_BASE         {0x3B, 0x48, 0x91, 0x26, 0x4A, 0x0B, 0x3E, 0x92, \
                                          0x26, 0x46, 0xEC, 0x63, 0x2E, 0x70, 0x33, 0x70}
#define CUSTOM_SERVICE_UUID              0x1400
#define CUSTOM_VALUE_CHAR_UUID           0x1500

// Length of the data in bytes
#define DATA_LENGTH                      4

// Forward declaration of the ble_cus_t type.
typedef struct ble_cus_s ble_cus_t; 

// Custom Service init structure. This contains all options and data needed for initialization of the service
typedef struct {
    uint8_t                       initial_custom_value;           // Initial custom value
    ble_srv_cccd_security_mode_t  custom_value_char_attr_md;     // Initial security level for Custom characteristics attribute
} ble_cus_init_t;

// Custom Service structure. This contains various status information for the service
struct ble_cus_s {
    uint16_t                      service_handle;                 // Handle of Custom Service (as provided by the BLE stack)
    ble_gatts_char_handles_t      custom_value_handles;           // Handles related to the Custom Value characteristic
    uint16_t                      conn_handle;                    // Handle of the current connection (as provided by the BLE stack, is BLE_CONN_HANDLE_INVALID if not in a connection)
    uint8_t                       uuid_type; 
};


// Function for initializing the Custom Service
uint32_t ble_cus_init(ble_cus_t * p_cus, const ble_cus_init_t * p_cus_init);


// Function for changing the service value
uint32_t ble_cus_custom_value_update(ble_cus_t * p_cus, uint8_t * custom_value);


#endif //BLE_CUS__