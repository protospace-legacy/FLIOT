## Sensors

The data pins used to connect to the microcontroller can be changed. The pins in the connection diagrams below are just used as an example.

#### Connection
The mq-9 and mq-135 sensors could be connected as shown in the diagram below.

<img src="https://i.imgur.com/MlKwcVw.png" width="400" height="300">

| μc pin | sensor pin |
| ------ | ----------:|
| 5V     | VCC        |
| GND    | GND        |
| DNC    | D0         |
| P0.27  | A0         |

The resistors are used as a voltage divider to drop the voltage from 5v to 3.3v because the input pins on the microcontroller only work with a maximum of 3.3v. The diodes are used to protect against overvoltage. This is done by using a method called diode clipping. This method prevents high voltages from reaching the microcontroller.

The bmp280 sensor could be connected as shown in the diagram below.

<img src="https://i.imgur.com/x0UKwkB.png" width="300" height="400">

| μc pin | sensor pin |
| ------ | ----------:|
| VDD    | VCC        |
| GND    | GND        |
| P0.26  | SCL        |
| P0.29  | SDA        |
| P0.30  | CSB        |
| P0.31  | SDO        |

The bmp280 can be usen with SPI and I2C. In this case the sensor is connected using SPI. There is no voltage divider needed as the sensor can operate on a voltage of 3.3v.

#### Creation

To create a sensor you want to create the corresponding sensor struct. These structs can be made with the macro functions found in the sensor header files or by creating and filling a struct manually. These structs are used as an argument in every function call and should be passed by reference.

```C
typedef struct mq9_config {
float R0;
nrf_saadc_input_t input_pin;
uint8_t channel_nr;
nrf_saadc_value_t buffer[1];
} mq9_config; 
```
```C
#define MQ9_CONFIG(adc_pin, channel) {   \
	.R0 = (float)0.69,               \
	.input_pin = (adc_pin),          \
	.channel_nr = (uint8_t)(channel) \
}
```

#### Initialization

To use the sensors you need to call the corresponding initialization function. These functions set the sensor up for use.

```C
void  bmp280_init(bmp280_config *config);
```
```C
void  mq135_init(mq135_config *config);
```
```C
void  mq9_init(mq9_config *config);
```

#### Calibration

The mq-9 and mq-135 sensors need to be calibrated before use. These sensors need to be in a "clean" environment when calibrating. This means that the air quality should be in its default state. Calibrating the sensors when the laser cutter is on would not be a good idea. The mq-9 and mq-135 can be calibrated with their respective functions.
```C
float  mq9_get_calibration(mq9_config *config);
```
```C
float  mq135_get_calibration(mq135_config *config);
```
These functions take a sensor struct as an argument. The calibration value in this struct (R0) changes when calling these functions. The functions also return the value as float. When calibrating these sensors you should keep calling the function until the returned value becomes stable. When you want to use the sensor you should make sure the R0 value in the sensor struct has been changed to the stable value you found before.

_Note:_
_The bmp280 does not need to be calibrated because the sensor has been calibrated at fabrication._

#### Usage

To use the sensors you need to use the read functions corresponding to the sensor. These functions return values in different units. The bmp280 returns a value in Pascal, the mq-135 returns a value in Parts per million and the mq-9 returns a value in a ratio RS/R0. This ratio can be converted to Parts per million by looking at the graph in the datasheet of the sensor.

```C
float  bmp280_read_pressure(bmp280_config *config);
```
```C
float  mq135_get_ppm(mq135_config *config);
```
```C
float  mq9_get_RSR0(mq9_config *config);
```

## BLE

BLE is used to send sensor values from the microcontroller to the connected device, in this case a mobile phone. 

#### Initialization
To use BLE you need to initialize it first, this is done by calling the initialization function. This function takes care of setting up everything needed for BLE communication.

```C
void  init_ble();
``` 

#### Services
In this implementation of BLE we use a separate service for every sensor. You can change te amount of services by changing the following line of code.

```C
#define  SENSOR_AMOUNT  15
```

#### Values
Every service has its own value where the sensordata is stored. This value consists of a uint8_t array with a pre-defined size. This size can be changed by adjusting the following line of code.

```C
#define  DATA_LENGTH  4
```
The amount 4 means that the array consists of 4 bytes (uint8_t). To change a value you can call the following function.

```C
void  ble_change_value(uint8_t sensor_number, uint8_t * value);
```

The sensor_number is used to determine which service value to change. If the SENSOR_AMOUNT is 6, sensor_number 0 corresponds with the first sensor and 5 with the last (just like arrays). The value argument should be a uint8_t array with the sensor value inside.

#### Connection
To connect to the microcontroller we use the nRF Connect application on a mobile phone. In the list of detected devices there should be a connection named FLIO. When you press connect you should see a list with all the services. When you press a service and press the corresponding downwards pointing arrow you should be able to see the service value in hexadecimal.

## Building & Flashing
To build and flash the code we use a Makefile. This Makefile is included in the working directory. In this Makefile you should add all the files that you want to compile (path to source files and header files). The Makefile is always accompanied by a linker file. When you want to build the project you should use the command __make__ in the command line. When you want to flash the code to the microcontroller you should use the command __make flash__. To use BLE on the microcontroller you need a SoftDevice. You can flash the SoftDevice by using the command __make flash_softdevice__. To erase the code from the mictocontroller you can use the command __make erase__.
