#include "mq135.h"

void mq135_init(mq135_config *config) {
    nrfx_saadc_config_t adc_config = NRFX_SAADC_DEFAULT_CONFIG;
    adc_config.resolution = NRF_SAADC_RESOLUTION_10BIT;
    adc_config.oversample = NRF_SAADC_OVERSAMPLE_2X;
    nrfx_saadc_init(&adc_config, NULL);
    nrf_saadc_channel_config_t channel_config = NRFX_SAADC_DEFAULT_CHANNEL_CONFIG_SE(config->input_pin);
    nrfx_saadc_channel_init(config->channel_nr, &channel_config);
}


// Function to calculate the correction factor based on temperature and humidity
float mq135_get_correction_factor(float t, float h) {
    // Linearization of the temperature dependency curve under and above 20 degree C
    // below 20degC: fact = a * t * t - b * t - (h - 33) * d
    // above 20degC: fact = a * t + b * h + c
    // this assumes a linear dependency on humidity
    if(t < 20){
        return CORA * t * t - CORB * t + CORC - (h-33.)*CORD;
    } else {
        return CORE * t + CORF * h + CORG;
    }
}


// Function to calculate the resistance of the sensor
float mq135_get_resistance(mq135_config *config) {
    nrf_saadc_value_t buf[1];
    nrfx_saadc_sample_convert(config->channel_nr, buf);
    return ((1023./(float)buf[0]) - 1.)*RLOAD;
}


// Function to calculate the resistance of the sensor based on temperature and humidity
float mq135_get_corrected_resistance(mq135_config *config, float t, float h) {
    return mq135_get_resistance(config)/mq135_get_correction_factor(t, h);
}


float mq135_get_ppm(mq135_config *config) {
    return PARA * pow((mq135_get_resistance(config)/config->R0), -PARB);
}


float mq135_get_corrected_ppm(mq135_config *config, float t, float h) {
    return PARA * pow((mq135_get_corrected_resistance(config, t, h)/config->R0), -PARB);
}


// Function to calculate the resistance of the sensor at atmospheric CO2 level
float mq135_get_R0(mq135_config *config) {
    return mq135_get_resistance(config) * pow((ATMOCO2/PARA), (1./PARB));
}


// Function to calculate the resistance of the sensor at atmospheric CO2 level
// based on temperature and humidity
float mq135_get_corrected_R0(mq135_config *config, float t, float h) {
    return mq135_get_corrected_resistance(config, t, h) * pow((ATMOCO2/PARA), (1./PARB));
}


float mq135_get_calibration(mq135_config *config) {
    config->R0 = mq135_get_R0(config);
    return config->R0;
}


float mq135_get_corrected_calibration(mq135_config *config, float t, float h) {
    config->R0 = mq135_get_corrected_R0(config, t, h);
    return config->R0;
}