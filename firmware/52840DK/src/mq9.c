#include "mq9.h"

void mq9_init(mq9_config *config) {
    nrfx_saadc_config_t adc_config = NRFX_SAADC_DEFAULT_CONFIG;
    adc_config.resolution = NRF_SAADC_RESOLUTION_10BIT;
    adc_config.oversample = NRF_SAADC_OVERSAMPLE_2X;
    nrfx_saadc_init(&adc_config, NULL);
    nrf_saadc_channel_config_t channel_config = NRFX_SAADC_DEFAULT_CHANNEL_CONFIG_SE(config->input_pin);
    nrfx_saadc_channel_init(config->channel_nr, &channel_config);
}


// Function to read the voltage supplied by the sensor
float mq9_read_voltage(mq9_config *config) {
    nrf_saadc_value_t buf[1];
    nrfx_saadc_sample_convert(config->channel_nr, buf);
    return buf[0] * (3.6/1024);
}


float mq9_get_calibration(mq9_config *config) {
    float sensor_volt = 0;  
    float RS_air = 0;
    for(int x = 0 ; x < 100 ; x++) { 
        sensor_volt = sensor_volt + mq9_read_voltage(config); 
    }
    sensor_volt = sensor_volt / 100.0; 
    RS_air = (3.6-sensor_volt)/sensor_volt;
    config->R0 = RS_air/9.9; // According to MQ9 datasheet table 
    return config->R0;
}


float mq9_get_RSR0(mq9_config *config) {
    float sensor_volt = 0; 
    float RS_gas = 0; 
    sensor_volt = mq9_read_voltage(config);
    RS_gas = (3.6 - sensor_volt) / sensor_volt; 
    return RS_gas / config->R0; // ratio = RS/R0
}