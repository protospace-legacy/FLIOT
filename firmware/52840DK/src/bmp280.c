#include "bmp280.h"

// Write 8 bits using SPI
void bmp280_write8(bmp280_config *config, uint8_t reg, uint8_t value) {
    uint8_t tmp_tx[] = {reg & ~0x80, value};
    uint8_t tmp_rx[sizeof(uint16_t)];
    nrfx_spim_xfer_desc_t xfer_desc = NRFX_SPIM_XFER_TRX(tmp_tx, sizeof(tmp_tx), tmp_rx, sizeof(tmp_rx));

    APP_ERROR_CHECK(nrfx_spim_xfer(&config->spi, &xfer_desc, 0));
    nrf_delay_us(100);
}


void bmp280_set_sampling(bmp280_config *config, 
                         uint8_t mode,
                         uint8_t tempSampling,
                         uint8_t pressSampling,
                         uint8_t filter,
                         uint8_t duration) {
    config->meas_reg.mode = mode;
    config->meas_reg.osrs_t = tempSampling;
    config->meas_reg.osrs_p = pressSampling;

    config->config_reg.filter = filter;
    config->config_reg.t_sb = duration;

    bmp280_write8(config, BMP280_REGISTER_CONFIG, (config->config_reg.t_sb << 5) | (config->config_reg.filter << 2) | config->config_reg.spi3w_en);
    bmp280_write8(config, BMP280_REGISTER_CONTROL, (config->meas_reg.osrs_t << 5) | (config->meas_reg.osrs_p << 2) | config->meas_reg.mode);
}


// Reads 24 bits from the sensor using SPI
uint32_t bmp280_read24(bmp280_config *config, uint8_t reg) {
    uint8_t tmp_tx[] = {(reg | 0x80), 0x00, 0x00, 0x00};
    uint8_t tmp_rx[sizeof(uint32_t)];
    nrfx_spim_xfer_desc_t xfer_desc = NRFX_SPIM_XFER_TRX(tmp_tx, sizeof(tmp_tx), tmp_rx, sizeof(tmp_rx));

    APP_ERROR_CHECK(nrfx_spim_xfer(&config->spi, &xfer_desc, 0));

    uint32_t value = tmp_rx[1];
    value = (value<<8) + tmp_rx[2];
    value = (value<<8) + tmp_rx[3];
    nrf_delay_us(100);
    return value;
}


// Reads 16 bits from the sensor using SPI
uint16_t bmp280_read16(bmp280_config *config, uint8_t reg) {
    uint8_t tmp_tx[] = {(reg | 0x80), 0x00, 0x00};
    uint8_t tmp_rx[sizeof(uint16_t) + 1];
    nrfx_spim_xfer_desc_t xfer_desc = NRFX_SPIM_XFER_TRX(tmp_tx, sizeof(tmp_tx), tmp_rx, sizeof(tmp_rx));

    APP_ERROR_CHECK(nrfx_spim_xfer(&config->spi, &xfer_desc, 0));
    nrf_delay_us(100);

    uint16_t value = tmp_rx[1];
    value = (value<<8) + tmp_rx[2];

    return value;
}


// Reads 16 bits from the sensor using SPI and returns it in Little-Endian 
uint16_t bmp280_read16_LE(bmp280_config *config, uint8_t reg) {
    uint16_t temp = bmp280_read16(config, reg);
    return (temp >> 8) | (temp << 8);
}


// Reads 16 bits from the sensor using SPI and returns it signed
int16_t bmp280_readS16(bmp280_config *config, uint8_t reg) {
    return (int16_t)bmp280_read16(config, reg);
}


// Reads 16 bits from the sensor using SPI and returns it signed in Little-Endian 
int16_t bmp280_readS16_LE(bmp280_config *config, uint8_t reg) {
    return (int16_t)bmp280_read16_LE(config, reg);
}


// Reads bmp280 registers for calibration
void bmp280_read_coefficients(bmp280_config *config) {
    config->calib.dig_T1 = bmp280_read16_LE(config, BMP280_REGISTER_DIG_T1);
    config->calib.dig_T2 = bmp280_readS16_LE(config, BMP280_REGISTER_DIG_T2);
    config->calib.dig_T3 = bmp280_readS16_LE(config, BMP280_REGISTER_DIG_T3);

    config->calib.dig_P1 = bmp280_read16_LE(config, BMP280_REGISTER_DIG_P1);
    config->calib.dig_P2 = bmp280_readS16_LE(config, BMP280_REGISTER_DIG_P2);
    config->calib.dig_P3 = bmp280_readS16_LE(config, BMP280_REGISTER_DIG_P3);
    config->calib.dig_P4 = bmp280_readS16_LE(config, BMP280_REGISTER_DIG_P4);
    config->calib.dig_P5 = bmp280_readS16_LE(config, BMP280_REGISTER_DIG_P5);
    config->calib.dig_P6 = bmp280_readS16_LE(config, BMP280_REGISTER_DIG_P6);
    config->calib.dig_P7 = bmp280_readS16_LE(config, BMP280_REGISTER_DIG_P7);
    config->calib.dig_P8 = bmp280_readS16_LE(config, BMP280_REGISTER_DIG_P8);
    config->calib.dig_P9 = bmp280_readS16_LE(config, BMP280_REGISTER_DIG_P9);
}


float bmp280_read_temperature(bmp280_config *config) {
  int32_t var1, var2;

  int32_t adc_T = bmp280_read24(config, BMP280_REGISTER_TEMPDATA);
  adc_T >>= 4;

  var1 = ((((adc_T >> 3) - ((int32_t)config->calib.dig_T1 << 1))) *
          ((int32_t)config->calib.dig_T2)) >>
         11;

  var2 = (((((adc_T >> 4) - ((int32_t)config->calib.dig_T1)) *
            ((adc_T >> 4) - ((int32_t)config->calib.dig_T1))) >>
           12) *
          ((int32_t)config->calib.dig_T3)) >>
         14;

  config->t_fine = var1 + var2;

  float T = (config->t_fine * 5 + 128) >> 8;
  return T / 100;
}


float bmp280_read_pressure(bmp280_config *config) {
  int64_t var1, var2, p;

  // Must be done first to get the t_fine variable set up
  bmp280_read_temperature(config);

  int32_t adc_P = bmp280_read24(config, BMP280_REGISTER_PRESSUREDATA);
  adc_P >>= 4;

  var1 = ((int64_t)config->t_fine) - 128000;
  var2 = var1 * var1 * (int64_t)config->calib.dig_P6;
  var2 = var2 + ((var1 * (int64_t)config->calib.dig_P5) << 17);
  var2 = var2 + (((int64_t)config->calib.dig_P4) << 35);
  var1 = ((var1 * var1 * (int64_t)config->calib.dig_P3) >> 8) +
         ((var1 * (int64_t)config->calib.dig_P2) << 12);
  var1 =
      (((((int64_t)1) << 47) + var1)) * ((int64_t)config->calib.dig_P1) >> 33;

  if (var1 == 0) {
    return 0; // avoid exception caused by division by zero
  }
  p = 1048576 - adc_P;
  p = (((p << 31) - var2) * 3125) / var1;
  var1 = (((int64_t)config->calib.dig_P9) * (p >> 13) * (p >> 13)) >> 25;
  var2 = (((int64_t)config->calib.dig_P8) * p) >> 19;

  p = ((p + var1 + var2) >> 8) + (((int64_t)config->calib.dig_P7) << 4);
  return (float)p / 256;
}


void bmp280_init(bmp280_config *config)
{
    nrfx_spim_config_t spi_config = NRFX_SPIM_DEFAULT_CONFIG;
    spi_config.frequency      = NRF_SPIM_FREQ_500K;
    spi_config.ss_pin         = config->ss_pin;
    spi_config.miso_pin       = config->miso_pin;
    spi_config.mosi_pin       = config->mosi_pin;
    spi_config.sck_pin        = config->sck_pin;
    spi_config.use_hw_ss      = true;
    spi_config.ss_active_high = false;
    spi_config.bit_order      = NRF_SPIM_BIT_ORDER_MSB_FIRST;
    spi_config.mode           = NRF_SPIM_MODE_0;
    APP_ERROR_CHECK(nrfx_spim_init(&config->spi, &spi_config, NULL, NULL));
    
    bmp280_read_coefficients(config);
    bmp280_set_sampling(config,
                 MODE_NORMAL,
                 SAMPLING_X2,
                 SAMPLING_X16,
                 FILTER_X16,
                 STANDBY_MS_500);
}