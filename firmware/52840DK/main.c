#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "nrf_delay.h"
#include "mq9.h"
#include "mq135.h"
#include "bmp280.h"
#include "ble_con.h"
#include "nrf_delay.h"

APP_TIMER_DEF(m_repeated_timer_id);

uint8_t cus_value[4] = {0x00,0x00,0x00,0x00};

static void repeated_timer_handler(void * p_context) {
    cus_value[3]++;
    ble_change_value(0, cus_value);
}

static void create_timers() {
    ret_code_t err_code;

    // Create timers
    err_code = app_timer_create(&m_repeated_timer_id,
                                APP_TIMER_MODE_REPEATED,
                                repeated_timer_handler);
    APP_ERROR_CHECK(err_code);
}

static void log_init(void) {
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}

static void init_app_timer() {
    ret_code_t err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);
}

static void start_timers() {
    ret_code_t err_code = app_timer_start(m_repeated_timer_id, APP_TIMER_TICKS(1000), NULL);
    APP_ERROR_CHECK(err_code);
}

int main(void) {
    log_init();
    bool erase_bonds = true;
    init_app_timer();
    create_timers();
    start_timers();
    init_ble();

    // bmp280_config bmp280 = BMP280_CONFIG(26, 29, 30, 31, 3);
    // mq135_config mq135 = MQ135_CONFIG(NRF_SAADC_INPUT_AIN1, 0);
    mq9_config mq9 = MQ9_CONFIG(NRF_SAADC_INPUT_AIN0, 0);

    float ppm;

    // bmp280_init(&bmp280);
    // mq135_init(&mq135);
    mq9_init(&mq9);
  
    advertising_start(erase_bonds);

    // Enter main loop.
    for (;;) {
        sd_app_evt_wait();
      
        // ppm = bmp280_read_temperature(&bmp280);
        // ppm = mq135_get_ppm(&mq135);
        ppm = mq9_get_RSR0(&mq9);

        if(ppm){};
        // NRF_LOG_INFO("data: " NRF_LOG_FLOAT_MARKER, NRF_LOG_FLOAT(bmp280_read_pressure(&bmp280));
        // NRF_LOG_INFO("data: " NRF_LOG_FLOAT_MARKER, NRF_LOG_FLOAT(mq135_get_ppm(&mq135)));
        NRF_LOG_INFO("data: " NRF_LOG_FLOAT_MARKER, NRF_LOG_FLOAT(mq9_get_RSR0(&mq9)));

        NRF_LOG_FLUSH();
        nrf_delay_ms(1000);
    }
}