set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

set(COMMON_SOURCES ${CMAKE_CURRENT_LIST_DIR}/src/)
set(COMMON_INCLUDES ${CMAKE_CURRENT_LIST_DIR}/include/ ${CMAKE_CURRENT_LIST_DIR}/../external/hana/include/)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_C_STANDARD 11)
set(CMAKE_CXX_FLAGS -Wno-register)
set(COMMON_FLAGS -lc -lnosys -lm -Wall -Werror  -flto -ffunction-sections -fdata-sections -fno-builtin -O3 -fno-exceptions)
set(COMMON_LD_FLAGS   ${COMMON_FLAGS} -Wl,--gc-sections --specs=nano.specs)

enable_language(ASM)
enable_language(C)
