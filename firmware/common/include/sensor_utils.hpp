#pragma once
#include "sensor.hpp"
#include <boost/hana/ext/std/tuple.hpp>
#include <boost/hana/slice.hpp>
#include <boost/hana/tuple.hpp>

namespace{
//From https://stackoverflow.com/questions/34672441/stdis-base-of-for-template-classes
template <template <typename...> class C, typename...Ts>
std::true_type is_base_of_template_impl(const C<Ts...>*);

template <template <typename...> class C>
std::false_type is_base_of_template_impl(...);

template <typename T, template <typename...> class C>
using is_base_of_template = decltype(is_base_of_template_impl<C>(std::declval<T*>()));
}


template<typename S, typename T>
void serialize_impl(const T& sensor, std::ostream& ostream, bool last){
    if constexpr(is_base_of_template<T, Sensor>::value){
        S::pre_sensor(ostream);
        S::sensor(sensor, ostream);
        if(last){
            S::post_last_sensor(ostream);
        }else{
            S::post_sensor(ostream);
        }
    }
    else if constexpr(is_base_of_template<T, SensorPackage>::value){
        S::pre_pack(sensor, ostream);

        auto slice = boost::hana::slice_c<0, std::tuple_size_v<T> - 1>(sensor.sensors);
        std::apply(
                [&](auto &...ts){
                    (serialize_impl<S>(ts, ostream, false), ...);
                    },
                slice);
        serialize_impl<S>(std::get<std::tuple_size_v<T> - 1>(sensor.sensors), ostream, true);

        S::post_pack(sensor, ostream);
    }
}

template<typename S, typename... T>
void serialize(std::tuple<T...> sensors, std::ostream & ostream){
    S::start(ostream);

    auto slice = boost::hana::slice_c<0, sizeof...(T) - 1>(sensors);
    std::apply(
            [&](auto &...ts){
                (serialize_impl<S>(ts, ostream, false), ...);
                },
            slice);
    serialize_impl<S>(std::get<sizeof...(T) - 1>(sensors), ostream, true);

    S::end(ostream);
}

template<typename T, typename S>
void sensor_for_each_impl(const T& sensor, S serializer){
    if constexpr(is_base_of_template<T, Sensor>::value){
        serializer(sensor);
    }
    else if constexpr(is_base_of_template<T, SensorPackage>::value){
        std::apply(
                [&](auto &...ts){(sensor_for_each_impl(ts, serializer), ...);},
                sensor.sensors);
    }
}

template<typename... T, typename S>
void sensor_for_each(std::tuple<T...> sensors,S serializer){
    std::apply(
            [&](auto &...ts){(sensor_for_each_impl(ts, serializer), ...);},
            sensors);
}

struct json_serializer{
    constexpr static auto start = [](std::ostream& ostream){ostream << "{";};
    constexpr static auto pre_pack = [](const auto& pack,std::ostream& ostream){ostream << pack.name << ":{";};
    constexpr static auto pre_sensor = [](std::ostream& ostream){};
    constexpr static auto sensor = [](const auto &sensor, std::ostream& ostream){ostream << "\"" << sensor.name << "\": { \"value\":\"" << sensor.get() << "\", \"symbol\":\"" << sensor.symbol << "\"}";};
    constexpr static auto post_sensor = [](std::ostream& ostream){ ostream << ",";};
    constexpr static auto post_last_sensor = [](std::ostream& ostream){};
    constexpr static auto post_pack = [](std::ostream& ostream){ostream << "},";};
    constexpr static auto end = [](std::ostream& ostream){ostream << "}";};
};
