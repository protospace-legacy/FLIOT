#pragma once
#include <streambuf>

//From https://stackoverflow.com/questions/1494182/setting-the-internal-buffer-used-by-a-standard-stream-pubsetbuf
template <typename char_type>
struct ostreambuf : public std::basic_streambuf<char_type, std::char_traits<char_type> >
{
    char_type* buffer;
    std::streamsize bufferLength;
    ostreambuf(char_type* buffer, std::streamsize bufferLength):
        buffer(buffer),
        bufferLength(bufferLength)
    {
        // set the "put" pointer the start of the buffer and record it's length.
        this->setp(buffer, buffer + bufferLength);
    };

    void reset(){
        this->setp(buffer, buffer + bufferLength);
    };
};
