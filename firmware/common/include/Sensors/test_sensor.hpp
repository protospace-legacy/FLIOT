#include "../sensor.hpp"

struct TestSensor : public Sensor<char>{

    TestSensor(const char* name):Sensor(name, ""){};

    value_t get() const{
        return 'A';
    }
};
