/**
 * @file sensor.hpp
 * @author Robert Bezem (robert.bezem@gmail.com)
 * @brief Base sensor implementation
 * @version 0.1
 * @date 2019-02-12
 * 
 * @copyright Copyright (c) 2019
 * 
 */
#pragma once

#include <iostream>
#include <tuple>

/**
 * @brief Interface for all sensor types
 * 
 */
struct SensorBase{};

/**
 * @brief Single Sensor Base class
 * 
 * @tparam T type of sensor reading value
 */
template <typename T>
struct Sensor : public SensorBase
{
    /**
    * @brief Type of sensor reading value
    * 
    */
    using value_t = T;

    /**
    * @brief Name of sensor
    * 
    */
    const char *name;

    /**
     * @brief Symbol/Unit of sensor
     * 
     */
    const char *symbol;

    /**
    * @brief Construct a new Sensor object
    * 
    * @param name name of sensor
    * @param symbol symbol/unit of sensor
    */
    Sensor(const char *name, const char *symbol) : name(name), symbol(symbol){};

    /**
     * @brief Retrieve current value of sensor
     * This is marked const because it should be logical const
     * physical const is not required acording to isocpp guidelines
     * https://isocpp.org/wiki/faq/const-correctness#logical-vs-physical-const 
     * @return value_t current value of sensor
     */
    virtual value_t get() const = 0;
};

/**
 * @brief Package of multiple sensors
 * 
 * @tparam T variadic template pack of @see Sensors
 */
template <typename... T>
struct SensorPackage : public SensorBase
{
    const char* name;
    /**
 * @brief All sensors contained in this package
 * 
 */
    std::tuple<T...> sensors;

    /**
 * @brief Construct a new Sensor Package object
 * 
 * @param pack Sensors that this package contains
 */
    SensorPackage(const char* name, T... pack) : name(name), sensors(pack...){};
};
