Firmware for FLIOT devices
==========================


Setting up build environment
------

### Prerequisits
- arm-gcc-none-eabi
- cmake > 3.4

#### Platform dependent prerequisists
##### NRF52
- nrfjprog
- nrf52 sdk placed in `external/nrf_sdk`
  dont forget to specify your version in `common/nrf.cmake`

### Setting up
Start by making a build directory `mkdir build` then go into that build directory `cd build` and run `cmake ..` to genrate the build files.
