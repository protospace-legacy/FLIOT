#include <nrf_gpio.h>
#include <nrfx_uart.h>
#include <Sensors/test_sensor.hpp>
#include <ostreambuf.hpp>

const uint32_t led_1_pin = NRF_GPIO_PIN_MAP(0,8);
const uint32_t led_2_pin = NRF_GPIO_PIN_MAP(0,9);
const uint32_t led_3_pin = NRF_GPIO_PIN_MAP(0,10);
const uint32_t delay     = 10000;

const nrfx_uart_config_t uart_params={
    NRF_GPIO_PIN_MAP(0,19),//RX_PIN
    NRF_GPIO_PIN_MAP(0,18),//TX_PIN
    NRF_UART_PSEL_DISCONNECTED,
    NRF_UART_PSEL_DISCONNECTED,
    nullptr,
    NRF_UART_HWFC_DISABLED,
    NRF_UART_PARITY_EXCLUDED,
    NRF_UART_BAUDRATE_115200,
    NRFX_UART_DEFAULT_CONFIG_IRQ_PRIORITY
};

void sleep(size_t cycles){
    for(size_t i = 0; i<cycles; i++){
        asm("nop");
    }
};

const nrfx_uart_t instance = NRFX_UART_INSTANCE(0);

std::array<char, 10> serial_tx_buffer;
ostreambuf<char> ostreamBuffer(serial_tx_buffer.data(), 10);
std::ostream messageStream(&ostreamBuffer);

int main(){
    TestSensor t("Test1");

    nrf_gpio_cfg_output(led_1_pin);
        nrf_gpio_pin_toggle(led_1_pin);
    nrf_gpio_cfg_output(led_2_pin);
        nrf_gpio_pin_toggle(led_2_pin);
    nrf_gpio_cfg_output(led_3_pin);
        nrf_gpio_pin_toggle(led_3_pin);
    nrfx_uart_init(&instance, &uart_params, nullptr);

    while(true){
        nrf_gpio_pin_toggle(led_1_pin);
        sleep(delay);
        nrf_gpio_pin_toggle(led_2_pin);
        sleep(delay);
        nrf_gpio_pin_toggle(led_3_pin);
        sleep(delay);
        nrfx_uart_tx(&instance, (uint8_t const*)"Cycled\r\n", sizeof("Cycled\r\n"));
    }
    return 0;
}
